#!/usr/bin/bash
# SPDX-License-Identifier: LGPL-2.1-or-later

check() {
    require_binaries osk-sdl || return 1
    
    return 255
}

depends() {
    return 0
}

install() {
    inst_multiple -o \
         $systemdsystemunitdir/osk-sdl-ask-password.path \
         $systemdsystemunitdir/osk-sdl-ask-password.service \
         $systemdsystemunitdir/sysinit.target.wants/osk-sdl-ask-password.path \
         /usr/share/fonts/dejavu-sans-fonts/DejaVuSans.ttf \
         /lib/systemd/systemd-reply-password \
         /etc/osk.conf \
         /usr/lib*/dri/* \
         /usr/lib*/libdrm.so* \
         /usr/lib*/libgbm.so* \
         /usr/lib*/libEGL*.so* \
         /usr/lib*/libGLES*.so* \
         /usr/lib*/libGL*.so* \
         /usr/share/glvnd/egl_vendor.d/* \
         osk-sdl \
         cut
    inst_simple "$moddir/osk-sdl-ask-password.sh" /usr/bin/osk-sdl-ask-password
    
    $SYSTEMCTL -q --root "$initdir" mask systemd-ask-password-console.service || :
    $SYSTEMCTL -q --root "$initdir" mask systemd-ask-password-plymouth.service || :
    $SYSTEMCTL -q --root "$initdir" mask systemd-ask-password-console.path || :
    $SYSTEMCTL -q --root "$initdir" mask systemd-ask-password-plymouth.path || :
}

