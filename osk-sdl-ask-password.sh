#!/usr/bin/bash
# SPDX-License-Identifier: LGPL-2.1-or-later

export SDL_VIDEODRIVER=kmsdrm

plymouth hide-splash 2>/dev/null

for file in `ls /run/systemd/ask-password/ask.*`; do
  socket="$(cat "$file" | grep "Socket=" | cut -d= -f2)"
  /usr/bin/osk-sdl -d x -n x -k | /lib/systemd/systemd-reply-password 1 "$socket"
done

plymouth show-splash 2>/dev/null

