%define dracutlibdir %{_prefix}/lib/dracut

Name:           osk-sdl
Version:        0.66
Release:        1%{?dist}
Summary:        Lightweight On-Screen-Keyboard based on SDL2

License:        GPLv3+
URL:            https://gitlab.com/postmarketOS/osk-sdl
Source0:        https://gitlab.com/postmarketOS/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

Source1:        osk-sdl-dracut-module-setup.sh
Source2:        osk-sdl-ask-password.sh
Source3:        osk-sdl-ask-password.path
Source4:        osk-sdl-ask-password.service

Patch0:         https://gitlab.com/postmarketOS/osk-sdl/-/commit/613a3702bed9b8aebae6bf6c83fbe7110756160f.patch#/osk-sdk-0.66-array-include.patch
Patch1:         https://gitlab.com/postmarketOS/osk-sdl/-/commit/41f67e26b8e8466c0613ea51ff602c19e01c7009.patch#/osk-sdk-0.66-algorithm-include.patch

BuildRequires:  gcc-c++
BuildRequires:  meson
BuildRequires:  pkgconfig(scdoc)
BuildRequires:  pkgconfig(sdl2)
BuildRequires:  pkgconfig(SDL2_ttf)
BuildRequires:  pkgconfig(libcryptsetup)
BuildRequires:  /usr/bin/xvfb-run
BuildRequires:  dos2unix
Requires:       dejavu-sans-fonts

%description
On-screen-keyboard used to unlock the encrypted root partition on touchscreen devices like phones
or tablets

%package dracut
Summary: Integration of OSK-SDL and Dracut
Requires: %{name} = %{version}-%{release}
Requires: dracut

%description dracut
Provides a Dracut module that will ask for password with an on-screen-keyboard

%prep
%autosetup -p1 -n %{name}-%{version}
sed -i "s|/usr/share/fonts/ttf-dejavu/|%{_datadir}/fonts/dejavu-sans-fonts/|" osk.conf
unix2dos osk.conf

%build
%meson
%meson_build

%install
%meson_install
mkdir -p %{buildroot}%{dracutlibdir}/modules.d/10osk-sdl
install -p -m 0644 %{SOURCE1} %{buildroot}%{dracutlibdir}/modules.d/10osk-sdl/module-setup.sh
install -p -m 0755 %{SOURCE2} %{buildroot}%{dracutlibdir}/modules.d/10osk-sdl/osk-sdl-ask-password.sh
mkdir -p %{buildroot}%{_unitdir}/sysinit.target.wants
install -p -m 0755 %{SOURCE3} %{buildroot}%{_unitdir}/osk-sdl-ask-password.path
install -p -m 0755 %{SOURCE4} %{buildroot}%{_unitdir}/osk-sdl-ask-password.service
ln -s ../osk-sdl-ask-password.path %{buildroot}%{_unitdir}/sysinit.target.wants/

# Fedora xvfb-run doesn't support --auto-display, yet it advertises as if it does
# %%check
# %%meson_test

%files
%{_sysconfdir}/osk.conf
%{_bindir}/osk-sdl
%{_mandir}/man*/*
%license LICENSE
%doc README.md

%files dracut
%{dracutlibdir}/modules.d/10osk-sdl
%{_unitdir}/osk-sdl-ask-password.path
%{_unitdir}/osk-sdl-ask-password.service
%{_unitdir}/sysinit.target.wants/osk-sdl-ask-password.path

%changelog
* Sun Jun 19 2022 marcin <marcin@ipv8.pl> - 0.66-1
- Initial release
